import React, {Component} from 'react';
import {Image, View} from 'react-native';

import PDFView from './../Components/PDFViewer/PDFView';

const PDFViewer = (props) => {
  return (
      <View>
          <PDFView uri={'http://www.africau.edu/images/default/sample.pdf'} />
      </View>
  )
};

export default PDFViewer;
