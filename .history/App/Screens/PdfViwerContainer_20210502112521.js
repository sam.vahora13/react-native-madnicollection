import React, {Component} from 'react';
import {Image, View} from 'react-native';

import PDFView from './../Components/PDFViewer/PDFView';

const PDFViewer = () => {
  return (
      <View>
          <PDFView />
      </View>
  )
};

export default PDFViewer;
