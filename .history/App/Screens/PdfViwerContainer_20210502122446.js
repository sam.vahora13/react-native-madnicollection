import React, {Component} from 'react';
import {Image, View, Text} from 'react-native';
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Body,
  } from 'native-base';

import PDFView from './../Components/PDFViewer/PDFView';

const PDFViewer = (props) => {
  return (
    <Container>
        <Content padder>
            <PDFView uri={'http://www.africau.edu/images/default/sample.pdf'} />
            {/* <Text>teste</Text> */}
        </Content>
    </Container>
  )
};

export default PDFViewer;
