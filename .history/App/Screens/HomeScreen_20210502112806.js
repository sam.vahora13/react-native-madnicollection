/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */

import React, {Component} from 'react';
import {Image} from 'react-native';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Body,
} from 'native-base';

import PDFView from './../Components/PDFViewer/PDFView';

const HomeScreen = () => {
  return (
    <Container>
      <Header />
      <Content padder>
        <Card>
          <CardItem header onPress={() => alert('This is Card Header')}>
            <Text>Sureh fatiya</Text>
          </CardItem>
          <CardItem
            button
            onPress={() => {
              alert('This is Card Body');
            }}>
            <Body>
              <Text>Click on any carditem</Text>
              {/* <PDFView /> */}
            </Body>
          </CardItem>
          <CardItem footer onPress={() => alert('This is Card Footer')}>
            <Text>End</Text>
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
};

export default HomeScreen;
