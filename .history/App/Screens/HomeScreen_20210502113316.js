/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */

import React from 'react';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Body,
} from 'native-base';

import PDFView from './../Components/PDFViewer/PDFView';
import {useNavigation} from 'react-navigation-hooks';

import PdfViwerContainer from './../Screens/PdfViwerContainer';

const HomeScreen = () => {

  const {navigate} = useNavigation();

  return (
    <Container>
      <Header />
      <Content padder>
        <Card>
          <CardItem header onPress={() => alert('This is Card Header')}>
            <Text>Sureh fatiya</Text>
          </CardItem>
          <CardItem
            button
            onPress={() => {
              // alert('This is Card Body');
              navigate('OtpVerificationPhone');
            }}>
            <Body>
              <Text>Click on any carditem</Text>
              {/* <PDFView /> */}
            </Body>
          </CardItem>
          <CardItem footer onPress={() => alert('This is Card Footer')}>
            <Text>End</Text>
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
};

export default HomeScreen;
