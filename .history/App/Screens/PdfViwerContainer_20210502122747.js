import React, {Component, useState, useEffect} from 'react';
import {Image, View, Text, BackHandler} from 'react-native';
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Body,
  } from 'native-base';

import PDFView from './../Components/PDFViewer/PDFView';

const PDFViewer = (props) => {

    useEffect(() => {
        // Your code here
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        // returned function will be called on component unmount
        return () => {
            BackHandler.removeEventListener(
            'hardwareBackPress',
            handleBackButtonClick,
            );
        };
    }, []);

    const handleBackButtonClick = () => {
        props.navigation.goBack(null);
        return true;
    };

  return (
    <Container>
        <Content padder>
            <PDFView uri={'http://www.africau.edu/images/default/sample.pdf'} />
            {/* <Text>teste</Text> */}
        </Content>
    </Container>
  )
};

export default PDFViewer;
