import React from 'react';
import {Image, TouchableOpacity, PixelRatio} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';

import HomeScreen from '../Screens/HomeScreen';
import PdfViwerContainer from '../Screens/PdfViwerContainer';

const AppNavigator = createAnimatedSwitchNavigator(
  {
    HomeScreen: {screen: HomeScreen},
    PdfViwerContainer: {screen: PdfViwerContainer},
    // AuthLoadingScreen: AuthLoadingScreen,
    // App: AppStack,
    // Auth: AuthStack,
    // AppDirections: {screen: AppDirections},
    // LoginWithPhone: {screen: LoginWithPhone},
    // OtpVerificationPhone: {screen: OtpVerificationPhone},
    // DriverDashNavigator: DriverDashNavigator,
    // SignUp: {screen: SignUp},
    // // StaffMemberList: {screen: StaffMemberList},
    // ChangeUserMobileNo: {screen: ChangeUserMobileNo},
    // RecordCustomPayments: {screen: RecordCustomPayments},
    // NewUserNavigations: NewUserNavigations,
    // // // NewUserNavigations: {screen: NewUserNavigations},
    // UserIndividualReport: {screen: UserIndividualReport},
    // UserLiveTrackMap: {screen: UserLiveTrackMap},
    // managerTabNavigator: managerTabNavigator,
    // UserProfileUpdate: {screen: UserProfileUpdate},
    // Home: {screen: Login},
    // UserType: {screen: UserType},
    // EmpCode: {screen: EmpCode},
    // StaffList: {screen: StaffList},
    // ManagerDashboardNavigator: ManagerDashboardNavigator,
    // DriverDashboardNavigator: DriverDashboardNavigator,
  },
  // {
  //   initialRouteName: 'AuthLoadingScreen',
  //   headerMode: 'none',
  //   navigationOptions: {
  //     headerVisible: false,
  //   },
  //   transition: (
  //     <Transition.Together>
  //       <Transition.Out type="fade" durationMs={400} interpolation="easeIn" />
  //       <Transition.In type="fade" durationMs={500} />
  //     </Transition.Together>
  //   ),
  // },
);

export default createAppContainer(AppNavigator);
